import argparse
import joblib
import pandas as pd
import os
import warnings


warnings.filterwarnings("ignore", category=UserWarning)


class predict_health(object):
 
    def __init__(self):
        pass
 
    def predict(self,data):

        model = "model.pkl"
        inputdata = data

        self.clf = joblib.load("./" + model)

        # Extract value of X
        dataset = inputdata.split(':')
        featurearray=[float(i) for i in dataset]
        rowdf = pd.DataFrame([featurearray], columns=['Height', 'Weight'])
        predictions = self.clf.predict(rowdf)
        print(predictions)
        return predictions


def main():

    data = os.environ.get("INPUT_DATA")
    if data is None:
        parser = argparse.ArgumentParser()
        parser.add_argument('-data', help='prediction data set', default='')
        args = parser.parse_args()
        data = args.data
    obj = predict_health()
    obj.predict(data)


if __name__== "__main__":
    main()


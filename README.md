# Health Predictor

Health Predictor is used to determine the health index of an individual given the current height and weight. The model is based on regression and returns an index value between 0 and 5 to indicate the following
* 0 - Extremely Weak
* 1 - Weak
* 2 - Normal
* 3 - Overweight
* 4 - Obesity
* 5 - Extreme obesity

## Contents

`predict_health.py` - Regression model to predict health index.
##### Parameters
* input data - ":" separated value of height and weight.

## Workflow

### Save Data

#### Prediction Data

The following example shows sample s3Path that points to the folder where prediction data is stored.

    s3Path - healthpredictor
    Files -
        model.pkl

The following example shows what input data looks like. 

   [69:150]

### Run Model

#### Prediction

### Use Results


